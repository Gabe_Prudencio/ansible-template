# README #

This is a docker container containing the configuration for an Ansible Control Node. The Ansible Control Node will
connect to the Ansible Host, and apply the commands in the stated Playbook.

### Install and Configuration ###

* Clone the repository and create a ansible-template/config folder
* Create an "authorized_keys" file in the ansible-template/config folder
* Add the keys of the users you want to have access to the servers, to the config/authorized_keys file
* Create an ansible-template/ssh folder in the ansible-docker folder
* Generate and copy both the private and public ssh key into the ansible-template/ssh folder
    - Note: Do not worry about setting permissions as the install script does that for you
* Add the Ansible public ssh key to the server(s) you wish to configure
* Create an "inventory" file in the ansible-template/config folder
    - Note: Format the inventory file as follows
    - $HOSTNAME ansible_host=$IP ansible_user=$USER
* Add hostname/ips of the servers you wish to configure to the config/inventory file
* Run the install script to create the docker container

### Running a Playbook ###

* Running a Playbook requires four variables
    - User: User to ssh into the server running the Ansible-Control-Node
    - IP: IP of the server running the Ansible-Control-Node
    - Group: The group of servers that Ansible will run the playbook on
        - Default is all servers in inventory file: 'all'
    - Playbook: The playbook that is to be run. Playbooks can be found in the ansible-template/playbooks folder
* The ansible-template/scripts/run_playbook.sh script will be capable of running all playbooks in the ansible-template/playbooks folder

