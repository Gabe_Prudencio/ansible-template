#!/bin/bash

ls /home/$USER/ansible/ansible-vault/vault/passwords/*-password.txt > ./envlist.txt

while read line
do 
TEMP_ENV=$(echo $line | sed 's/-password.txt//' | sed 's/\///g' | sed 's/home'$USER'ansibleansible-vaultvaultpasswords//')
if [ ! -z $TEMP_ENV ]; then
ssh -q $USER@$IP "docker exec ansible-control-node ansible-vault encrypt /ansible/vault/passwords/$TEMP_ENV-password.txt --vault-id admin@/ansible/vault/passwords/admin-password.txt"
fi
done < ./envlist.txt
rm ./envlist.txt
ssh -q $USER@$IP "rm /home/$USER/ansible/ansible-vault/vault/passwords/admin-password.txt"
